#include "Complex.h"

Complex::Complex()
{
	Complex(0, 0);
}

Complex::Complex(double x, double y)
{
	this->x = x;
	this->y = y;
}

Complex::Complex(Complex x, Complex y)
{
	this->x = x.x + y.x;
	this->y = x.y + y.y;
}

Complex::Complex(double x)
{
	this->x = x;
	this->y = 0;
}

double Complex::Module()
{
	return sqrt(this->x * this->x + this->y * this->y);
}

Complex::operator double()
{
	return this->x;
}

Complex Complex::operator!()
{
	return Complex(this->x, -this->y);
}

Complex Complex::operator+(Complex added)
{
	return Complex(this->x + added.x, this->y + added.y);
}

Complex Complex::operator-()
{
	return Complex(-this->x, -this->y);
}

Complex Complex::operator-(Complex added)
{
	return Complex(*this) + -added;
}

Complex Complex::operator*(Complex b)
{
	return Complex(this->x * b.x - this->y * b.y, this->x * b.y + this->y * b.x);
}

Complex Complex::operator/(double b)
{
	return Complex(this->x / b, this->y / b);
}

Complex Complex::operator/(Complex b)
{
	return (*this * !b / (double(b * (!b))));
}

Complex Complex::operator=(int b)
{
	this->x = b;
	this->y = 0;
	return *this;
}

Complex Complex::operator=(double b)
{
	this->x = b;
	this->y = 0;
	return *this;
}

bool Complex::operator==(Complex& b)
{
	if (b.x == this->x && b.y == this->y)
		return true;
	return false;
}

bool Complex::operator!=(Complex& b)
{
	if (*this == b)
		return false;
	return true;
}

void Complex::prt()
{
	cout << this->x << ((y < 0) ? "" : "+") << this->y << "*i";
}

double Complex::getReal()
{
	return this->x;
}

double Complex::getImag()
{
	return this->y;
}
