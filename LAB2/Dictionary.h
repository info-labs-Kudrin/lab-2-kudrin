#pragma once
#include <map>
#include <vector>
#include <string>

using namespace std;

class Dictionary
{
public:
	Dictionary();
	Dictionary(string text);
	~Dictionary();
	void setText(string text);
	string getText();
	void work();
	void printWords();
private:
	string text;
	map<string, vector<int>> dict;
};

Dictionary::Dictionary() { this->text = ""; }

Dictionary::Dictionary(string text) { this->text = text; }

Dictionary::~Dictionary() {}

void Dictionary::setText(string text) { this->text = text; }

string Dictionary::getText() { return this->text; }

void Dictionary::work()
{
	string cur = "";
	int i = 0, size = text.size();
	while (i != size)
	{
		if (this->text[i] == ' ' && cur != "")
		{
			this->dict[cur].push_back(i - cur.size());
			cur = "";
		}
		else
		{
			cur = cur + this->text[i];
		}
		i++;
	}
	if (cur != "")
		this->dict[cur].push_back(i - cur.size());
}

void Dictionary::printWords()
{
	printf("Found %d words (first comes word, then the first indexex in the text):\n", this->dict.size());
	for (size_t i = 0; i < dict.size(); i++)
	{
		auto c = dict.begin();
		for (size_t j = 0; j < i; j++)
			c++;
		cout << "\"" << c->first << "\": ";
		for (size_t j = 0; j < c->second.size(); j++)
			printf("%3d ", c->second[j]);
		cout << endl;
	}
}