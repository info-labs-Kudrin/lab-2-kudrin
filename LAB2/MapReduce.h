#pragma once
//#include <iostream>
#include"Sequence.h"
#include<functional>

const int ERROR_DIFFERENT_SIZES = 1;

template<class T>
ArraySequence<T>* Map(ArraySequence<T>* s1, ArraySequence<T>* s2, std::function<T(T, T)> f)
{
	ArraySequence<T>* res = new ArraySequence<T>();
	int size = s1->getSize();
	for (int i = 0; i < size; i++)
		res->Append(f(s1->get(i), s2->get(i)));
	return res;
}

template<class T>
ArraySequence<T>* Map(ArraySequence<T>* s, std::function<T(T)> f)
{
	ArraySequence<T>* res = new ArraySequence<T>();
	int size = s->getSize();
	for (int i = 0; i < size; i++)
		res->Append(f(s->get(i)));
	return res;
}

template<class T>
T* Reduce(ArraySequence<T>* s, std::function<T(T, T)> f, const T& startValue)
{
	T* res = new T(startValue);
	int size = s->getSize();
	for (int i = 0; i < size; i++)
		(*res) = f(*res, (*s)[i]);
	return res;
}

template<class T>
T* MapReduce(ArraySequence<T>* s1, ArraySequence<T>* s2, std::function < T(T, T, T)> f, const T& startValue)
{
	int size = s1->getSize();
	if (size != s2->getSize())
		throw ERROR_DIFFERENT_SIZES;
	T* res = new T(startValue);
	for (int i = 0; i < size; i++)
	{
		*res = f(*res, (*s1)[i], (*s2)[i]);
	}
	return res;
}