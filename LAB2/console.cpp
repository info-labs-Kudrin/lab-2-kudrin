#include "console.h"
#include "Sequence.h"
#include "SquareMatrix.h"
#include "Dictionary.h"

template<class matrixType>
void forget(matrixType** theMatrixes)
{
	delete[](*theMatrixes);
}

template<class matrixType>
void prtMatrixes(matrixType* theMatrixes, int size)
{
	if (size == 0)
	{
		cout << "None matrixes exsist\n";
		return;
	}
	for (int i = 0; i < size; i++)
	{
		cout << i << ":\n";
		int matSize = theMatrixes[i].getSize();
		for (int j = 0; j < matSize; j++)
		{
			for (int k = 0; k < matSize; k++)
			{
				cout << theMatrixes[i].get(j, k) << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
}

template<>
void prtMatrixes<SquareMatrix<Complex>>(SquareMatrix<Complex >* theMatrixes, int size)
{
	if (size == 0)
	{
		cout << "None matrixes exsist\n";
		return;
	}
	for (int i = 0; i < size; i++)
	{
		cout << i << ":\n";
		int matSize = theMatrixes[i].getSize();
		for (int j = 0; j < matSize; j++)
		{
			for (int k = 0; k < matSize; k++)
			{
				theMatrixes[i].get(j, k).prt();
				cout << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
}

template<>
void prtMatrixes<ListedSquareMatrix<Complex>>(ListedSquareMatrix<Complex >* theMatrixes, int size)
{
	if (size == 0)
	{
		cout << "None matrixes exsist\n";
		return;
	}
	for (int i = 0; i < size; i++)
	{
		cout << i << ":\n";
		int matSize = theMatrixes[i].getSize();
		for (int j = 0; j < matSize; j++)
		{
			for (int k = 0; k < matSize; k++)
			{
				theMatrixes[i].get(j, k).prt();
				cout << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
}

template<class T>
void getEL(T& el)
{
	cin >> el;
}

template<>
void getEL<Complex>(Complex& el)
{
	double re, im;
	cin >> re >> im;
	el = Complex(re, im);
}

template<class T>
void apndMatrix(T** theMatrixes, int* size)
{
	cout << "Size: ";
	int matrixSize;
	cin >> matrixSize;
	if (matrixSize < 0 || matrixSize > 999999)
		throw ERROR_WRONG_SIZE;
	cout << endl << matrixSize << "*" << matrixSize << " elements (if they are to be complex, type real then imaginary):" << endl;
	T* newMat = new T(matrixSize);
	for (int i = 0; i < matrixSize; i++)
	{
		for (int j = 0; j < matrixSize; j++)
		{
			auto el = newMat->get(0, 0);
			getEL(el);
			newMat->set(el, i, j);
		}
	}
	(*theMatrixes)[*size - 1] = T(*newMat);
	delete newMat;
}

template<class T>
void crtMatrix(T** theMatrixes, int* size)
{
	T** res = new T*;
	*res = new T[*size + 1];
	for (int i = 0; i < *size; i++)
		(*res)[i] = T((*theMatrixes)[i]);
	(*size)++;
	apndMatrix<T>(res, size);
	forget<T>(theMatrixes);
	*theMatrixes = *res;
}

template<class T>
void sumMatrixes(T** theMatrixes, int* size)
{
	cout << "Type index 1 and index 2 (different):\n";
	size_t index1, index2;
	cin >> index1 >> index2;
	if (index1 == index2 || index1 >= *size || index2 >= *size)
		throw ERROR_WRONG_INDEX;
	(*theMatrixes)[index1] + ((*theMatrixes)[index2]);
}

template<class T>
void scalarMult(T** theMatrixes, int size)
{
	cout << "Type index then the scalar (if complex then first real then imaginary)\n";
	size_t index;
	cin >> index;
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	auto scalar = (*theMatrixes)[index].get(0, 0);
	getEL(scalar);
	(*theMatrixes)[index] * scalar;
}

template<class T>
void multMatrixes(T** theMatrixes, int size)
{
	cout << "Type index 1 and index 2 (different):\n";
	size_t index1, index2;
	cin >> index1 >> index2;
	if (index1 == index2 || index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	(*theMatrixes)[index1] * ((*theMatrixes)[index2]);
}

template<class T>
void sumCol(T** theMatrixes, int size)
{
	cout << "Type index of the matrix\n";
	size_t index;
	cin >> index;
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	cout << "Type index 1 and index 2 (different):\n";
	size_t index1, index2;
	cin >> index1 >> index2;
	size = (*theMatrixes)[index].getSize();
	if (index1 == index2 || index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	(*theMatrixes)[index].sumColumns(index1, index2);
}

template<class T>
void sumRow(T** theMatrixes, int size)
{
	cout << "Type index of the matrix\n";
	size_t index;
	cin >> index;
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	cout << "Type index 1 and index 2 (different):\n";
	size_t index1, index2;
	cin >> index1 >> index2;
	size = (*theMatrixes)[index].getSize();
	if (index1 == index2 || index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	(*theMatrixes)[index].sumRows(index1, index2);
}

template<class T>
void chgCol(T** theMatrixes, int size)
{
	cout << "Type index of the matrix\n";
	size_t index;
	cin >> index;
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	cout << "Type index 1 and index 2 (different):\n";
	size_t index1, index2;
	cin >> index1 >> index2;
	size = (*theMatrixes)[index].getSize();
	if (index1 == index2 || index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	(*theMatrixes)[index].ChangeColumns(index1, index2);
}

template<class T>
void chgRow(T** theMatrixes, int size)
{
	cout << "Type index of the matrix\n";
	size_t index;
	cin >> index;
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	cout << "Type index 1 and index 2 (different):\n";
	size_t index1, index2;
	cin >> index1 >> index2;
	size = (*theMatrixes)[index].getSize();
	if (index1 == index2 || index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	(*theMatrixes)[index].ChangeRows(index1, index2);
}

template<class T>
void sclCol(T** theMatrixes, int size)
{
	cout << "Type index of the matrix\n";
	size_t index;
	cin >> index;
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	if ((*theMatrixes)[index].getSize() == 0)
	{
		cout << "Empty matrix\n";
		return;
	}
	cout << "Type index then the scalar (if complex then first real then imaginary)\n";
	size_t index1;
	cin >> index1;
	size = (*theMatrixes)[index].getSize();
	if (index1 >= size)
		throw ERROR_WRONG_INDEX;
	auto scalar = (*theMatrixes)[index].get(0, 0);
	getEL(scalar);
	(*theMatrixes)[index].multiplyColumnBy(index1, scalar);
}

template<class T>
void sclRow(T** theMatrixes, int size)
{
	cout << "Type index of the matrix\n";
	size_t index;
	cin >> index;
	size = (*theMatrixes)[index].getSize();
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	if ((*theMatrixes)[index].getSize() == 0)
	{
		cout << "Empty matrix\n";
		return;
	}
	cout << "Type index then the scalar (if complex then first real then imaginary)\n";
	size_t index1;
	cin >> index1;
	if (index1 >= size)
		throw ERROR_WRONG_INDEX;
	auto scalar = (*theMatrixes)[index].get(0, 0);
	getEL(scalar);
	(*theMatrixes)[index].multiplyRowBy(index1, scalar);
}

template<class matrixType>
void matrixes()
{
	matrixType* theMatrixes = 0;
	int size = 0;
	bool brk = 0;
	while (1)
	{
		cout << "****************\nWhat do I do with matrixes?\n0 -- quit\n1--print matrixes\n2--create a matrix\n3--sum matrixes\n4--multiply a matrix by a scalar\n5--multiply matrixes\n6--sum columns\n7--sum rows\n8--change columns\n9--change rows\n10--multyply column by a scalar\n11--multiply row by a scalar\n";
		int choice;
		cin >> choice;
		switch (choice)
		{
		case 0:
		{
			brk = 1;
			break;
		}
		case 1:
		{
			prtMatrixes<matrixType>(theMatrixes, size);
			break;
		}
		case 2:
		{
			crtMatrix<matrixType>(&theMatrixes, &size);
			break;
		}
		case 3:
		{
			sumMatrixes<matrixType>(&theMatrixes, &size);
			break;
		}
		case 4:
		{
			scalarMult<matrixType>(&theMatrixes, size);
			break;
		}
		case 5:
		{
			multMatrixes<matrixType>(&theMatrixes, size);
			break;
		}
		case 6:
		{
			sumCol < matrixType>(&theMatrixes, size);
			break;
		}
		case 7:
		{
			sumRow < matrixType>(&theMatrixes, size);
			break;
		}
		case 8:
		{
			chgCol < matrixType>(&theMatrixes, size);
			break;
		}
		case 9:
		{
			chgRow < matrixType>(&theMatrixes, size);
			break;
		}
		case 10:
		{
			sclCol < matrixType>(&theMatrixes, size);
			break;
		}
		case 11:
		{
			sclRow < matrixType>(&theMatrixes, size);
			break;
		}
		default:
			break;
		}
		if (brk)
			break;
	}
}

void console()
{
	cout << "****************\nWhat do I do?\n1--dictionary task\n2--work with matrixes\n";
	int choice;
	cin >> choice;
	switch (choice)
	{
	case 1:
	{
		string text;
		cout << "----------------\nPrint some text in a line\n";
		char c = getchar();
		getline(cin, text);
		Dictionary a(text);
		a.work();
		cout << a.getText() << endl;
		a.printWords();
		break;
	}
	case 2:
	{
		cout << "What kind of matrixes am I going to use\n1--based on arrays\n2--based on linked lists\n";
		cin >> choice;
		if (choice == 1)
		{
			cout << "What kind of coefficients am I going to use\n1 -- int\n2 -- double\n3 -- complex\n";
			cin >> choice;
			switch (choice)
			{
			case 1:
			{
				matrixes<SquareMatrix<int>>();
				break;
			}
			case 2:
			{
				matrixes<SquareMatrix<double>>();
				break;
			}
			case 3:
			{
				matrixes<SquareMatrix<Complex>>();
				break;
			}
			default:
				break;
			}
		}
		else
		{
			cout << "What kind of coefficients am I going to use\n1 -- int\n2 -- double\n3 -- complex\n";
			cin >> choice;
			switch (choice)
			{
			case 1:
			{
				matrixes<ListedSquareMatrix<int>>();
				break;
			}
			case 2:
			{
				matrixes<ListedSquareMatrix<double>>();
				break;
			}
			case 3:
			{
				matrixes<ListedSquareMatrix<Complex>>();
				break;
			}
			default:
				break;
			}
		}
		break;
	}
	}
}