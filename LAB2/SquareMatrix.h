#pragma once
#include"Sequence.h"
#include <xkeycheck.h>

const int ERROR_WRONG_SIZE = 1;
const int ERROR_WRONG_INDEX = 2;

template <class T>
class SquareMatrix
{
private:
	ArraySequence<ArraySequence<T>> theMatrix;
	size_t size;
public:
	SquareMatrix();
	SquareMatrix(size_t size);
	SquareMatrix(size_t size, T standard);
	SquareMatrix(const SquareMatrix<T>& a);

	void addToThis(SquareMatrix<T>& aMatrix);
	void operator+(SquareMatrix<T>& aMatrix);

	void multiplyThisBy(const T& scalar);
	void operator*(const T& scalar);

	size_t getSize();
	T get(size_t index1, size_t index2);
	void set(T value, size_t index1, size_t index2);

	void multiplyThisBy(SquareMatrix<T>& aMatrix);
	void operator*(SquareMatrix<T>& aMatrix);
	
	void sumColumns(size_t index, size_t added);
	void sumRows(size_t index, size_t added);
	void multiplyColumnBy(size_t index, T scalar);
	void multiplyRowBy(size_t index, T scalar);
	void ChangeColumns(size_t index1, size_t index2);
	void ChangeRows(size_t index1, size_t index2);

	//Finds max element
	T normaMax();

	bool operator==(SquareMatrix<T>& a);
	bool operator!=(SquareMatrix<T>& a);

	ArraySequence<T> getColumn(size_t index);
	ArraySequence<T> getRow(size_t index);
	ArraySequence<T> operator[](size_t index);
};


template<class T>
inline SquareMatrix<T>::SquareMatrix() : SquareMatrix(0) {}

template<class T>
inline SquareMatrix<T>::SquareMatrix(size_t size) : SquareMatrix(size, T(0)) {}

template<class T>
inline SquareMatrix<T>::SquareMatrix(size_t size, T standard) : theMatrix(ArraySequence<ArraySequence<T>>(size, ArraySequence<T>(size, standard)))
{
	this->size = size;
}

template<class T>
inline SquareMatrix<T>::SquareMatrix(const SquareMatrix<T>& a) : theMatrix(a.theMatrix)
{
	this->size = a.size;
}

template<class T>
inline void SquareMatrix<T>::addToThis(SquareMatrix<T>& aMatrix)
{
	if (this->size != aMatrix.size)
		throw ERROR_WRONG_SIZE;
	for (size_t i = 0; i < size; i++)
		for(size_t j = 0; j < size; j++)
			this->theMatrix[i][j] = this->theMatrix[i][j] + aMatrix.theMatrix[i][j];
}

template<class T>
inline void SquareMatrix<T>::operator+(SquareMatrix<T>& aMatrix)
{
	this->addToThis(aMatrix);
}

template<class T>
inline void SquareMatrix<T>::multiplyThisBy(const T& scalar)
{
	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++)
			this->theMatrix[i][j] = this->theMatrix[i][j] * scalar;
}

template<class T>
inline void SquareMatrix<T>::operator*(const T& scalar)
{
	this->multiplyThisBy(scalar);
}

template <class T>
inline size_t SquareMatrix<T>::getSize()
{
	return this->size;
}

template <class T>
inline T SquareMatrix<T>::get(size_t index1, size_t index2)
{
	if (index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	return this->theMatrix[index1][index2];
}

template<class T>
inline void SquareMatrix<T>::set(T value, size_t index1, size_t index2)
{
	if (index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	this->theMatrix[index1][index2] = value;
}

template<class T>
inline void SquareMatrix<T>::multiplyThisBy(SquareMatrix<T>& aMatrix)
{
	if (this->size != aMatrix.size)
		throw ERROR_WRONG_SIZE;
	ArraySequence<ArraySequence<T>> res(size, ArraySequence<T>(size, T(0)));
	for (size_t i = 0; i < size; i++)
	{
		for (size_t j = 0; j < size; j++)
		{
			for (size_t k = 0; k < size; k++)
				res[i][j] = res[i][j] + this->theMatrix[i][k] * aMatrix.theMatrix[k][j];
		}
	}
	this->theMatrix = res;
}

template<class T>
inline void SquareMatrix<T>::operator*(SquareMatrix<T>& aMatrix)
{
	this->multiplyThisBy(aMatrix);
}

template<class T>
inline void SquareMatrix<T>::sumColumns(size_t index, size_t added)
{
	if (index >= size || added >= size)
		throw ERROR_WRONG_INDEX;
	for (size_t i = 0; i < size; i++)
		this->theMatrix[i][index] = this->theMatrix[i][index] + this->theMatrix[i][added];
}

template<class T>
inline void SquareMatrix<T>::sumRows(size_t index, size_t added)
{
	if (index >= size || added >= size)
		throw ERROR_WRONG_INDEX;
	for (size_t i = 0; i < size; i++)
		this->theMatrix[index][i] = this->theMatrix[index][i] + this->theMatrix[added][i];
}

template<class T>
inline void SquareMatrix<T>::multiplyColumnBy(size_t index, T scalar)
{
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	for (size_t i = 0; i < size; i++)
		this->theMatrix[i][index] = this->theMatrix[i][index] * scalar;
}

template<class T>
inline void SquareMatrix<T>::multiplyRowBy(size_t index, T scalar)
{
	for (size_t i = 0; i < size; i++)
		this->theMatrix[index][i] = this->theMatrix[index][i] * scalar;
}

template<class T>
inline void SquareMatrix<T>::ChangeColumns(size_t index1, size_t index2)
{
	if (index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	T buffer;
	for (size_t i = 0; i < size; i++)
	{
		buffer = this->theMatrix[i][index1];
		this->theMatrix[i][index1] = this->theMatrix[i][index2];
		this->theMatrix[i][index2] = buffer;
	}
}

template<class T>
inline void SquareMatrix<T>::ChangeRows(size_t index1, size_t index2)
{
	if (index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	T buffer;
	for (size_t i = 0; i < size; i++)
	{
		buffer = this->theMatrix[index1][i];
		this->theMatrix[index1][i] = this->theMatrix[index2][i];
		this->theMatrix[index2][i] = buffer;
	}
}

template<class T>
inline T SquareMatrix<T>::normaMax()
{
	#define mod(a) ((a < 0) ? -a : a)
	T res = this->theMatrix[0][0];
	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++)
			if (mod(this->theMatrix[i][j]) > res)
				res = mod(this->theMatrix[i][j]);
	return res;
}
template<>
inline Complex SquareMatrix<Complex>::normaMax()
{
	double res = this->theMatrix[0][0].Module();
	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++)
			if (res < this->theMatrix[i][j].Module())
				res = this->theMatrix[i][j].Module();
	return res;
}

template<class T>
inline bool SquareMatrix<T>::operator==(SquareMatrix<T>& a)
{
	if (this->size != a.size)
		return 0;
	for (size_t i = 0; i < size; i++)
		if (this->theMatrix[i] != a.theMatrix[i])
			return 0;
	return 1;
}

template<class T>
inline bool SquareMatrix<T>::operator!=(SquareMatrix<T>& a)
{
	return !(this->theMatrix == a.theMatrix);
}

template<class T>
inline ArraySequence<T> SquareMatrix<T>::getColumn(size_t index)
{
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	ArraySequence<T> res(size);
	for (size_t i = 0; i < size; i++)
		res[i] = this->theMatrix[i][index];
	return res;
}

template<class T>
inline ArraySequence<T> SquareMatrix<T>::getRow(size_t index)
{
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	return this->theMatrix[index];
}

template<class T>
inline ArraySequence<T> SquareMatrix<T>::operator[](size_t index)
{
	return this->getRow(index);
}


/*_________________________________________________
* LINKED VERSION
* Due to many reasons
* This couldn't have been done with polymorphism
* The main reason was not getting enough sleep
* Also there were troubles with the compliler
* Thanks for the attention
* _________________________________________________
*/

template <class T>
class ListedSquareMatrix
{
private:
	LListSequence<LListSequence<T>> theMatrix;
	size_t size;
public:
	ListedSquareMatrix();
	ListedSquareMatrix(size_t size);
	ListedSquareMatrix(size_t size, T standard);

	void addToThis(ListedSquareMatrix<T>& aMatrix);
	void operator+(ListedSquareMatrix<T>& aMatrix);

	void multiplyThisBy(const T& scalar);
	void operator*(const T& scalar);

	size_t getSize();
	T get(size_t index1, size_t index2);
	void set(T value, size_t index1, size_t index2);

	void multiplyThisBy(ListedSquareMatrix<T>& aMatrix);
	void operator*(ListedSquareMatrix<T>& aMatrix);

	void sumColumns(size_t index, size_t added);
	void sumRows(size_t index, size_t added);
	void multiplyColumnBy(size_t index, T scalar);
	void multiplyRowBy(size_t index, T scalar);
	void ChangeColumns(size_t index1, size_t index2);
	void ChangeRows(size_t index1, size_t index2);

	//Finds max element
	T normaMax();

	bool operator==(ListedSquareMatrix<T>& a);
	bool operator!=(ListedSquareMatrix<T>& a);

	LListSequence<T> getColumn(size_t index);
	LListSequence<T> getRow(size_t index);
	LListSequence<T> operator[](size_t index);
};


template<class T>
inline ListedSquareMatrix<T>::ListedSquareMatrix() : ListedSquareMatrix(0) {}

template<class T>
inline ListedSquareMatrix<T>::ListedSquareMatrix(size_t size) : ListedSquareMatrix(size, T(0)) {}

template<class T>
inline ListedSquareMatrix<T>::ListedSquareMatrix(size_t size, T standard) : theMatrix(LListSequence<LListSequence<T>>(size, LListSequence<T>(size, standard)))
{
	this->size = size;
}

template<class T>
inline void ListedSquareMatrix<T>::addToThis(ListedSquareMatrix<T>& aMatrix)
{
	if (this->size != aMatrix.size)
		throw ERROR_WRONG_SIZE;
	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++)
			this->theMatrix[i][j] = this->theMatrix[i][j] + aMatrix.theMatrix[i][j];
}

template<class T>
inline void ListedSquareMatrix<T>::operator+(ListedSquareMatrix<T>& aMatrix)
{
	this->addToThis(aMatrix);
}

template<class T>
inline void ListedSquareMatrix<T>::multiplyThisBy(const T& scalar)
{
	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++)
			this->theMatrix[i][j] = this->theMatrix[i][j] * scalar;
}

template<class T>
inline void ListedSquareMatrix<T>::operator*(const T& scalar)
{
	this->multiplyThisBy(scalar);
}

template <class T>
inline size_t ListedSquareMatrix<T>::getSize()
{
	return this->size;
}

template <class T>
inline T ListedSquareMatrix<T>::get(size_t index1, size_t index2)
{
	if (index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	return this->theMatrix[index1][index2];
}

template<class T>
inline void ListedSquareMatrix<T>::set(T value, size_t index1, size_t index2)
{
	if (index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	this->theMatrix[index1][index2] = value;
}

template<class T>
inline void ListedSquareMatrix<T>::multiplyThisBy(ListedSquareMatrix<T>& aMatrix)
{
	if (this->size != aMatrix.size)
		throw ERROR_WRONG_SIZE;
	LListSequence<LListSequence<T>> res(size, LListSequence<T>(size, T(0)));
	for (size_t i = 0; i < size; i++)
	{
		for (size_t j = 0; j < size; j++)
		{
			for (size_t k = 0; k < size; k++)
				res[i][j] = res[i][j] + this->theMatrix[i][k] * aMatrix.theMatrix[k][j];
		}
	}
	this->theMatrix = res;
}

template<class T>
inline void ListedSquareMatrix<T>::operator*(ListedSquareMatrix<T>& aMatrix)
{
	this->multiplyThisBy(aMatrix);
}

template<class T>
inline void ListedSquareMatrix<T>::sumColumns(size_t index, size_t added)
{
	if (index >= size || added >= size)
		throw ERROR_WRONG_INDEX;
	for (size_t i = 0; i < size; i++)
		this->theMatrix[i][index] = this->theMatrix[i][index] + this->theMatrix[i][added];
}

template<class T>
inline void ListedSquareMatrix<T>::sumRows(size_t index, size_t added)
{
	if (index >= size || added >= size)
		throw ERROR_WRONG_INDEX;
	for (size_t i = 0; i < size; i++)
		this->theMatrix[index][i] = this->theMatrix[index][i] + this->theMatrix[added][i];
}

template<class T>
inline void ListedSquareMatrix<T>::multiplyColumnBy(size_t index, T scalar)
{
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	for (size_t i = 0; i < size; i++)
		this->theMatrix[i][index] = this->theMatrix[i][index] * scalar;
}

template<class T>
inline void ListedSquareMatrix<T>::multiplyRowBy(size_t index, T scalar)
{
	for (size_t i = 0; i < size; i++)
		this->theMatrix[index][i] = this->theMatrix[index][i] * scalar;
}

template<class T>
inline void ListedSquareMatrix<T>::ChangeColumns(size_t index1, size_t index2)
{
	if (index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	T buffer;
	for (size_t i = 0; i < size; i++)
	{
		buffer = this->theMatrix[i][index1];
		this->theMatrix[i][index1] = this->theMatrix[i][index2];
		this->theMatrix[i][index2] = buffer;
	}
}

template<class T>
inline void ListedSquareMatrix<T>::ChangeRows(size_t index1, size_t index2)
{
	if (index1 >= size || index2 >= size)
		throw ERROR_WRONG_INDEX;
	T buffer;
	for (size_t i = 0; i < size; i++)
	{
		buffer = this->theMatrix[index1][i];
		this->theMatrix[index1][i] = this->theMatrix[index2][i];
		this->theMatrix[index2][i] = buffer;
	}
}

template<class T>
inline T ListedSquareMatrix<T>::normaMax()
{
#define mod(a) ((a < 0) ? -a : a)
	T res = this->theMatrix[0][0];
	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++)
			if (mod(this->theMatrix[i][j]) > res)
				res = mod(this->theMatrix[i][j]);
	return res;
}
template<>
inline Complex ListedSquareMatrix<Complex>::normaMax()
{
	double res = this->theMatrix[0][0].Module();
	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++)
			if (res < this->theMatrix[i][j].Module())
				res = this->theMatrix[i][j].Module();
	return res;
}

template<class T>
inline bool ListedSquareMatrix<T>::operator==(ListedSquareMatrix<T>& a)
{
	if (this->size != a.size)
		return 0;
	for (size_t i = 0; i < size; i++)
		if (this->theMatrix[i] != a.theMatrix[i])
			return 0;
	return 1;
}

template<class T>
inline bool ListedSquareMatrix<T>::operator!=(ListedSquareMatrix<T>& a)
{
	return !(this->theMatrix == a.theMatrix);
}

template<class T>
inline LListSequence<T> ListedSquareMatrix<T>::getColumn(size_t index)
{
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	LListSequence<T> res(size);
	for (size_t i = 0; i < size; i++)
		res[i] = this->theMatrix[i][index];
	return res;
}

template<class T>
inline LListSequence<T> ListedSquareMatrix<T>::getRow(size_t index)
{
	if (index >= size)
		throw ERROR_WRONG_INDEX;
	return this->theMatrix[index];
}

template<class T>
inline LListSequence<T> ListedSquareMatrix<T>::operator[](size_t index)
{
	return this->getRow(index);
}