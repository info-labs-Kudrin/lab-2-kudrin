#pragma once

#include"Complex.h"
#include"DynamicArray.h"
#include"LinkedList.h"

template <typename SequenceType, typename T>
class Sequence
{
protected:
	SequenceType items;
	Sequence(size_t size);
	Sequence(size_t size, T standard);
	Sequence();
	Sequence(T* items, int count);
	Sequence(const Sequence<SequenceType, T>& sq);
public:
	virtual ~Sequence() = 0;

	virtual T getFirst();
	virtual T getLast();
	T get(int index);
	void set(int index, const T& value);
	size_t getSize();
	void Append(T value);

	T& operator[](int index);
	void InsertAt(T value, int index);
	void Erase(int index);
	bool operator==(Sequence<SequenceType, T>& sq);
	bool operator!=(Sequence<SequenceType, T>& sq);
};

template <typename SequenceType, typename T>
Sequence<SequenceType, T>::~Sequence() {};



template<typename T>
class ArraySequence : public Sequence<DynamicArray<T>, T>
{
public:
	ArraySequence();
	ArraySequence(const ArraySequence<T>& sq);
	ArraySequence(T *items, int count);
	ArraySequence(size_t size);
	ArraySequence(size_t size, T standard);
	~ArraySequence() override;

	void resize(int size);
};

template <typename T>
class LListSequence : public Sequence<LinkedList<T>, T>
{
public:
	LListSequence();
	LListSequence(const LListSequence<T>& sq);
	LListSequence(T* items, int count);
	LListSequence(size_t size);
	LListSequence(size_t size, T standard);
	~LListSequence() override;

	void Prepend(T value);

	T getFirst() override;
	T getLast() override;

	//Concatenates list to this
	void Concat(LListSequence<T>& list);
	LListSequence<T>* getSubSequence(size_t startIndex, size_t endIndex);
};



//______________________________________
//======================================
//

template<typename SequenceType, typename T>
Sequence<SequenceType, T>::Sequence(size_t size) : items(size) { }

template<typename SequenceType, typename T>
inline Sequence<SequenceType, T>::Sequence(size_t size, T standard) : items(size, standard) {}
;

template<typename SequenceType, typename T>
Sequence<SequenceType, T>::Sequence() : items() { };

template<typename SequenceType, typename T>
Sequence<SequenceType, T>::Sequence(T* items, int count) : items(items, count) { };

template<typename SequenceType, typename T>
Sequence<SequenceType, T>::Sequence(const Sequence<SequenceType, T>& sq) : items(sq.items) { };

template<typename SequenceType, typename T>
T Sequence<SequenceType, T>::getFirst()
{
	return this->items[0];
}

template<typename SequenceType, typename T>
T Sequence<SequenceType, T>::getLast()
{
	return this->items[this->items.getSize() - 1];
}

template<typename SequenceType, typename T>
T Sequence<SequenceType, T>::get(int index)
{
	return this->items[index];
}

template<typename SequenceType, typename T>
void Sequence<SequenceType, T>::set(int index, const T& value)
{
	this->items.set(index, value);
}

template<typename SequenceType, typename T>
size_t Sequence<SequenceType, T>::getSize()
{
	return this->items.getSize();
}

template<typename SequenceType, typename T>
inline void Sequence<SequenceType, T>::Append(T value)
{
	this->items.Append(value);
}

template<typename SequenceType, typename T>
T& Sequence<SequenceType, T>::operator[](int index)
{
	return this->items[index];
}

template<typename SequenceType, typename T>
void Sequence<SequenceType, T>::InsertAt(T value, int index)
{
	this->items.InsertAt(value, index);
}

template<typename SequenceType, typename T>
void Sequence<SequenceType, T>::Erase(int index)
{
	this->items.Erase(index);
}

template<typename SequenceType, typename T>
bool Sequence<SequenceType, T>::operator==(Sequence<SequenceType, T>& sq)
{
	return (sq.items == this->items);
}

template<typename SequenceType, typename T>
inline bool Sequence<SequenceType, T>::operator!=(Sequence<SequenceType, T>& sq)
{
	return !(*this == sq);
}




template<typename T>
ArraySequence<T>::ArraySequence() : Sequence<DynamicArray<T>, T>() {};

template<typename T>
ArraySequence<T>::ArraySequence(T* items, int count) : Sequence<DynamicArray<T>, T>(items, count) {}

template<typename T>
ArraySequence<T>::ArraySequence(size_t size) : Sequence<DynamicArray<T>, T>(size) {}

template<typename T>
inline ArraySequence<T>::ArraySequence(size_t size, T standard) : Sequence<DynamicArray<T>, T>(size, standard){ }

template<typename T>
ArraySequence<T>::ArraySequence(const ArraySequence<T>& sq) : Sequence<DynamicArray<T>, T>(sq) {}

template<typename T>
ArraySequence<T>::~ArraySequence() {}

template<typename T>
void ArraySequence<T>::resize(int size)
{
	this->items.resize(size);
}




template<typename T>
LListSequence<T>::LListSequence() : Sequence<LinkedList<T>, T>() {}

template<typename T>
LListSequence<T>::LListSequence(T* items, int count) : Sequence<LinkedList<T>, T>(items, count) {}

template<typename T>
LListSequence<T>::LListSequence(size_t size) : Sequence<LinkedList<T>, T>(size) {}

template<typename T>
inline LListSequence<T>::LListSequence(size_t size, T standard) : Sequence<LinkedList<T>, T>(size, standard){}

template<typename T>
LListSequence<T>::LListSequence(const LListSequence<T>& sq) : Sequence<LinkedList<T>, T>(sq) {}

template<typename T>
LListSequence<T>::~LListSequence() {}

template<typename T>
void LListSequence<T>::Prepend(T value)
{
	this->items.Prepend(value);
}

template<typename T>
T LListSequence<T>::getFirst()
{
	return this->items.getFirst();
}

template<typename T>
T LListSequence<T>::getLast()
{
	return this->items.getLast();
}

template<typename T>
void LListSequence<T>::Concat(LListSequence<T>& list)
{
	this->items.Concat(list.items);
}

template<typename T>
inline LListSequence<T>* LListSequence<T>::getSubSequence(size_t startIndex, size_t endIndex)
{
	LListSequence<T>* res = new LListSequence<T>();
	res->items = *(this->items.GetSubList(startIndex, endIndex));
	return res;
}