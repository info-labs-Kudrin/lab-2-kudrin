#include "pch.h"
#include "CppUnitTest.h"
#include "../LAB2/MapReduce.h"
#include "../LAB2/Complex.h"
#include "../LAB2/DynamicArray.h"
#include "../LAB2/LinkedList.h"
#include "../LAB2/Sequence.h"
#include "../LAB2/SquareMatrix.h"
#include <cmath>
#include <functional>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Lab2Tests
{
	TEST_CLASS(Lab2Tests)
	{
	public:
		
		/*
		* TESTING
		* Complex.h
		*/
		TEST_METHOD(AA_Complex_InitTests)
		{
			Complex a(1, 1), b(1.1, 2.2), c(a, b), d(-1.2);
			Assert::IsTrue(a.getReal() == 1 && a.getImag() == 1);
			Assert::IsTrue(b.getReal() == 1.1 && b.getImag() == 2.2);
			Assert::IsTrue(c.getReal() == 2.1 && c.getImag() == 3.2);
			Assert::IsTrue(d.getReal() == -1.2 && d.getImag() == 0);
		}
		
		TEST_METHOD(AB_Complex_ArithmeticsTests)
		{
			Complex a(1.1, 1.1);

			Complex b = !a;
			Assert::IsTrue(b.getReal() == 1.1 && b.getImag() == -1.1);

			a = Complex(1.1, 1.1);
			b = Complex(1.1, -1.1);
			Complex c = a * b;
			Assert::IsTrue(c.getReal() == 2 * 1.1 * 1.1 && c.getImag() == 0);

			a = Complex(1, 2);
			b = Complex(-1, 4);
			c = a + b;
			Assert::IsTrue(c.getReal() == 0.0 && c.getImag() == 6.0);

			a = Complex(1, 2);
			b = Complex(-1, 4);
			c = a - b;
			Assert::IsTrue(c.getReal() == 2.0 && c.getImag() == -2.0);

			c = -b;
			Assert::IsTrue(c.getReal() == 1.0 && c.getImag() == -4.0);

			double m = b.Module();
			Assert::IsTrue(m == sqrt(17));

			m = 2.0;
			c = b / m;
			Assert::IsTrue(c.getReal() == -0.5 && c.getImag() == 2.0);

			c = a / b;
			Assert::IsTrue(a * !b / (double(b * !b)));

			a = Complex(1, 2);
			b = Complex(1, 2);
			c = Complex(1, 3);
			Assert::IsTrue(a == b);
			Assert::IsFalse(a == c);
			c = Complex(2, 2);
			Assert::IsFalse(a == c);
		}

		TEST_METHOD(AC_Complex_OtherMethodsTests)
		{
			Complex a(1.1, 2.2);
			double d = a.Module();
			Assert::IsTrue(d == sqrt(1.1 * 1.1 + 2.2 * 2.2));

			d = a;
			Assert::IsTrue(d == 1.1);

			a = 1;
			Assert::IsTrue(a.getReal() == 1 && a.getImag() == 0);
			a = 2.0;
			Assert::IsTrue(a.getReal() == 2.0 && a.getImag() == 0.0);
		}

		/*
		* END OF TESTING 
		* Complex.h
		* STARTING TESTING
		* DynamicArray.h
		*/

		TEST_METHOD(BA_DynamicArray_InitTests)
		{
			int s1 = 2, s2 = 3, s3 = 4;
			{
				DynamicArray<int> a1;
				Assert::IsTrue(a1.getSize() == 0);
				DynamicArray<double> a2;
				Assert::IsTrue(a2.getSize() == 0);
				DynamicArray<Complex> a3;
				Assert::IsTrue(a3.getSize() == 0);
			}
			{
				DynamicArray<int> a1(s1);
				DynamicArray<double> a2(s2);
				DynamicArray<Complex> a3(s3);

				Assert::IsTrue(a1.getSize() == 2);
				for (int i = 0; i < 2; i++)
					Assert::IsTrue(a1.get(i) == 0);
				Assert::IsTrue(a2.getSize() == 3);
				for (int i = 0; i < 3; i++)
					Assert::IsTrue(a2.get(i) == 0);
				Assert::IsTrue(a3.getSize() == 4);
				for (int i = 0; i < 4; i++)
					Assert::IsTrue(a3.get(i) == 0);
			}
			{
				int* m1 = new int[2];
				m1[0] = m1[1] = 1;

				double* m2 = new double[2];
				m2[0] = m2[1] = 2.0;

				Complex* m3 = new Complex[2];
				m3[0] = m3[1] = Complex(1.0, 2);

				DynamicArray<int> a1(m1, 2);
				DynamicArray<double> a2(m2, 2);
				DynamicArray<Complex> a3(m3, 2);

				Assert::IsTrue(a1.getSize() == 2);
				Assert::IsTrue(a1.get(0) == 1 && a1.get(1) == 1);
				Assert::IsTrue(a2.getSize() == 2);
				Assert::IsTrue(a2.get(0) == 2.0 && a2.get(1) == 2.0);
				Assert::IsTrue(a3.getSize() == 2);
				Assert::IsTrue(a3.get(0).getReal() == 1.0 && a3.get(0).getImag() == 2);
				Assert::IsTrue(a3.get(1).getReal() == 1.0 && a3.get(1).getImag() == 2);

				DynamicArray<int> b1(a1);
				DynamicArray<double> b2(a2);
				DynamicArray<Complex> b3(a3);
				Assert::IsTrue(a1 == b1 && a2 == b2 && a3 == b3);

				delete[] m1;
				delete[] m2;
				delete[] m3;
			}
		}
		
		TEST_METHOD(BB_DynamicArray_GetSetTests)
		{
			int* m1 = new int[2];
			m1[0] = m1[1] = 1;

			double* m2 = new double[2];
			m2[0] = m2[1] = 2.0;

			Complex* m3 = new Complex[2];
			m3[0] = m3[1] = Complex(1.0, 2);

			DynamicArray<int> a1(m1, 2);
			DynamicArray<double> a2(m2, 2);
			DynamicArray<Complex> a3(m3, 2);

			a1.set(0, -1);
			Assert::IsTrue(a1.get(0) == -1 && a1.get(1) == 1);

			a2.set(0, -1.0);
			Assert::IsTrue(a2.get(0) == -1.0 && a2.get(1) == 2.0);

			a3.set(0, Complex(-1, -2));
			Assert::IsTrue(a3.get(0).getReal() == -1 && a3.get(0).getImag() == -2);
			Assert::IsTrue(a3.get(1).getReal() == 1.0 && a3.get(1).getImag() == 2);

			Assert::ExpectException<int>([this] { DynamicArray<int> a1(2); a1.set(-1, -1); });
			Assert::ExpectException<int>([this] { DynamicArray<double> a2(2); a2.set(-1, -1); });
			Assert::ExpectException<int>([this] { DynamicArray<Complex> a3(2); a3.set(-1, Complex(-1, -1)); });

			Assert::ExpectException<int>([this] { DynamicArray<int> a1(2); a1.get(-1); });
			Assert::ExpectException<int>([this] { DynamicArray<double> a2(2); a2.get(-1); });
			Assert::ExpectException<int>([this] { DynamicArray<Complex> a3(2); a3.get(-1); });

			Assert::ExpectException<int>([this] { DynamicArray<int> a1(2); a1.get(10); });
			Assert::ExpectException<int>([this] { DynamicArray<double> a2(2); a2.get(10); });
			Assert::ExpectException<int>([this] { DynamicArray<Complex> a3(2); a3.get(10); });
		}
		
		TEST_METHOD(BC_DynamicArray_ResizeTest)
		{
			int* m1 = new int[2];
			m1[0] = m1[1] = 1;

			double* m2 = new double[2];
			m2[0] = m2[1] = 2.0;

			Complex* m3 = new Complex[2];
			m3[0] = m3[1] = Complex(1.0, 2);

			DynamicArray<int> a1(m1, 2);
			DynamicArray<double> a2(m2, 2);
			DynamicArray<Complex> a3(m3, 2);

			a1.resize(3);
			Assert::IsTrue(a1.getSize() == 3 && a1.get(0) == 1 && a1.get(1) == 1 && a1.get(2) == 0);
			a1.resize(1);
			Assert::IsTrue(a1.getSize() == 1 && a1.get(0) == 1);

			a2.resize(3);
			Assert::IsTrue(a2.getSize() == 3 && a2.get(0) == 2.0 && a2.get(1) == 2.0 && a2.get(2) == 0);
			a2.resize(1);
			Assert::IsTrue(a2.getSize() == 1 && a2.get(0) == 2.0);

			a3.resize(3);
			Assert::IsTrue(a3.getSize() == 3 && a3.get(0) == Complex(1.0, 2) && a3.get(1) == Complex(1.0, 2) && a3.get(2) == Complex(0, 0));
			a3.resize(1);
			Assert::IsTrue(a3.getSize() == 1 && a3.get(0) == Complex(1.0, 2));


			Assert::ExpectException<int>([this] { DynamicArray<int> a1(2); a1.resize(-1); });
			Assert::ExpectException<int>([this] { DynamicArray<double> a2(2); a2.resize(-1); });
			Assert::ExpectException<int>([this] { DynamicArray<Complex> a3(2); a3.resize(-1); });
		}
		
		TEST_METHOD(BD_DynamicArray_IndexOperatorTest)
		{
			int* m1 = new int[2];
			m1[0] = m1[1] = 1;

			double* m2 = new double[2];
			m2[0] = m2[1] = 2.0;

			Complex* m3 = new Complex[2];
			m3[0] = m3[1] = Complex(1.0, 2);

			DynamicArray<int> a1(m1, 2);
			DynamicArray<double> a2(m2, 2);
			DynamicArray<Complex> a3(m3, 2);
			Assert::IsTrue(a1.get(0) == a1[0] && a1.get(1) == a1[1]);
			Assert::IsTrue(a2.get(0) == a2[0] && a2.get(1) == a2[1]);
			Assert::IsTrue(a2.get(0) == a2[0] && a2.get(1) == a2[1]);

			Assert::ExpectException<int>([this] { DynamicArray<int> a1(2); a1[2]; });
			Assert::ExpectException<int>([this] { DynamicArray<double> a2(2); a2[2]; });
			Assert::ExpectException<int>([this] { DynamicArray<Complex> a3(2); a3[2]; });

			a1[0] = -2;
			a2[0] = -2;
			a3[0] = Complex(-2, -2);
			Assert::IsTrue(a1[0] == -2 && a2[0] == -2 && a3[0] == Complex(-2, -2));
		}

		TEST_METHOD(BE_DynamicArray_EquationOperatorTest)
		{
			DynamicArray<int> a1(2), b1(2), c1(3);
			DynamicArray<double> a2(2), b2(2), c2(3);
			DynamicArray<Complex> a3(2), b3(2), c3(3);
			
			a1[0] = 1; a1[1] = 2;
			b1[0] = 1; b1[1] = 2;
			Assert::IsTrue(a1 == b1);
			a1[0] = -1;
			Assert::IsFalse(a1 == b1);

			a2[0] = 1; a2[0] = 2;
			b2[0] = 1; b2[0] = 2;
			Assert::IsTrue(a2 == b2);
			a2[0] = -1;
			Assert::IsFalse(a2 == b2);

			a3[0] = Complex(1, 2); a3[1] = Complex(2, 3);
			b3[0] = Complex(1, 2); b3[1] = Complex(2, 3);
			Assert::IsTrue(a3 == b3);
			a3[0] = Complex(1, 1);
			Assert::IsFalse(a3 == b3);

			Assert::IsFalse(a1 == c1);
			Assert::IsFalse(a2 == c2);
			Assert::IsFalse(a3 == c3);
		}
		
		TEST_METHOD(BF_DynamicArray_InsertAtEraseTests)
		{
			DynamicArray<int> a(2);
			a[0] = 0;
			a[1] = 1;
			a.InsertAt(-1, 1);
			Assert::IsTrue(a.getSize() == 3 && a[0] == 0 && a[1] == -1 && a[2] == 1);

			a.InsertAt(-2, 0);
			a.InsertAt(-3, 4);
			Assert::IsTrue(a.getSize() == 5 && a[0] == -2 && a[1] == 0 && a[2] == -1 && a[3] == 1 && a[4] == -3);
			a.Erase(4);
			Assert::IsTrue(a.getSize() == 4 && a[0] == -2 && a[1] == 0 && a[2] == -1 && a[3] == 1);
			a.Erase(0);
			a.Erase(1);
			Assert::IsTrue(a.getSize() == 2 && a[0] == 0 && a[1] == 1);

			Assert::ExpectException<int>([this] {DynamicArray<int> a(2); a.InsertAt(-1, -1); });
			Assert::ExpectException<int>([this] {DynamicArray<int> a(2); a.InsertAt(-1, 3); });
			Assert::ExpectException<int>([this] {DynamicArray<int> a(2); a.Erase(-1); });
			Assert::ExpectException<int>([this] {DynamicArray<int> a(2); a.Erase(2); });
		}

		/*
		* END OF TESTING
		* DynamicArray.h
		* STARTING TESTING
		* LinkedList.h
		*/

		TEST_METHOD(CA_LinkedList_InitTests)
		{
			{
				LinkedList<int> a;
				LinkedList<double> b;
				LinkedList<Complex> c;

				Assert::IsTrue(a.getSize() == 0 && b.getSize() == 0 && c.getSize() == 0);
			}
			{
				int* m1 = new int[2];
				m1[0] = m1[1] = 1;

				double* m2 = new double[2];
				m2[0] = m2[1] = 2.0;

				Complex* m3 = new Complex[2];
				m3[0] = m3[1] = Complex(1.0, 2);

				LinkedList<int> a(m1, 2);
				LinkedList<double> b(m2, 2);
				LinkedList<Complex> c(m3, 2);

				Assert::IsTrue(a.get(0) == m1[0] && a.get(1) == m1[1]);
				Assert::IsTrue(b.get(0) == m2[0] && b.get(1) == m2[1]);
				Assert::IsTrue(c.get(0) == m3[0] && c.get(1) == m3[1]);

				LinkedList<int> a1(a);
				LinkedList<double> b1(b);
				LinkedList<Complex> c1(c);
				Assert::IsTrue(a == a1 && b == b1 && c == c1);
				delete[] m1;
				delete[] m2;
				delete[] m3;
			}
			{
				LinkedList<int> a(2);
				LinkedList<Complex> b(2);
				Assert::IsTrue(a.getSize() == 2 && b.getSize() == 2 && a[0] == 0 && a[1] == 0 && b[0] == Complex(0) && b[1] == Complex(0));
			}
		}

		TEST_METHOD(CB_LinkedList_GetAppendPrependTests)
		{
			LinkedList<int> a;
			LinkedList<double> b;
			LinkedList<Complex> c;
			
			a.Append(1);
			a.Append(2);
			Assert::IsTrue(a.getSize() == 2 && a.get(0) == 1 && a.get(1) == 2);

			b.Append(1);
			b.Append(2);
			Assert::IsTrue(b.getSize() == 2 && b.get(0) == 1 && b.get(1) == 2);

			c.Append(Complex(1,1));
			c.Append(Complex(-1, 2));
			Assert::IsTrue(c.getSize() == 2 && c.get(0) == Complex(1, 1) && c.get(1) == Complex(-1, 2));

			a.Prepend(10);
			b.Prepend(10);
			c.Prepend(Complex(10, 11));
			Assert::IsTrue(a.getSize() == 3 && a.get(0) == 10 && a.get(2) == 2);
			Assert::IsTrue(b.getSize() == 3 && b.get(0) == 10 && b.get(2) == 2);
			Assert::IsTrue(c.getSize() == 3 && c.get(0) == Complex(10, 11) && c.get(2) == Complex(-1, 2));

			Assert::IsTrue(a.get(0) == a.getFirst() && b.get(0) == b.getFirst() && c.get(0) == c.getFirst());
			Assert::IsTrue(a.get(2) == a.getLast() && b.get(2) == b.getLast() && c.get(2) == c.getLast());

			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.getFirst(); });
			Assert::ExpectException<int>([this] { LinkedList<double> a1; a1.getFirst(); });
			Assert::ExpectException<int>([this] { LinkedList<Complex> a1; a1.getFirst(); });

			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.getLast(); });
			Assert::ExpectException<int>([this] { LinkedList<double> a1; a1.getLast(); });
			Assert::ExpectException<int>([this] { LinkedList<Complex> a1; a1.getLast(); });

			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.get(1); });
			Assert::ExpectException<int>([this] { LinkedList<double> a1; a1.get(1); });
			Assert::ExpectException<int>([this] { LinkedList<Complex> a1; a1.get(1); });
			
			a.set(0, -100);
			Assert::IsTrue(a[0] == -100);
		}

		TEST_METHOD(CC_LinkedList_EquationOperatorTest)
		{
			int* m1 = new int[2];
			m1[0] = m1[1] = 1;
			int* m11 = new int[3];
			m11[0] = m11[1] = m11[2] = 1;
			int* m111 = new int[3];
			m111[1] = m111[2] = 1;
			m111[0] = 0;

			double* m2 = new double[2];
			m2[0] = m2[1] = 2.0;
			double* m22 = new double[3];
			m22[0] = m22[1] = m22[2] = 2.0;
			double* m222 = new double[3];
			m222[1] = m222[2] = 2.0;
			m222[0] = 0;

			Complex* m3 = new Complex[2];
			m3[0] = m3[1] = Complex(1.0, 2);
			Complex* m33 = new Complex[3];
			m33[0] = m33[1] = m33[2] = Complex(1.0, 2);
			Complex* m333 = new Complex[3];
			m333[1] = m333[2] = Complex(1.0, 2);
			m333[0] = Complex(-1, 0);


			LinkedList<int> a1(m1, 2), a2(m1, 2), a3(m11, 3), a4(m111, 3);
			LinkedList<double> b1(m2, 2), b2(m2, 2), b3(m22, 3), b4(m222, 3);
			LinkedList<Complex> c1(m3, 2), c2(m3, 2), c3(m33, 3), c4(m333, 3);

			Assert::IsTrue(a1 == a2 && a2 == a1);
			Assert::IsFalse(a1 == a3 || a1 == a4);
			Assert::IsFalse(a3 == a4);

			Assert::IsTrue(b1 == b2 && b2 == b1);
			Assert::IsFalse(b1 == b3 || b1 == b4);
			Assert::IsFalse(b3 == b4);

			Assert::IsTrue(c1 == c2 && c2 == c1);
			Assert::IsFalse(c1 == c3 || c1 == c4);
			Assert::IsFalse(c3 == c4);

			delete[] m1;
			delete[] m11;
			delete[] m111;
			delete[] m2;
			delete[] m22;
			delete[] m222;
			delete[] m3;
			delete[] m33;
			delete[] m333;
		}

		TEST_METHOD(CD_LinkedList_GetSubListTest)
		{
			int* m1 = new int[2];
			m1[0] = m1[1] = 1;
			int* m11 = new int[3];
			m11[0] = m11[1] = m11[2] = 1;

			double* m2 = new double[2];
			m2[0] = m2[1] = 2.0;
			double* m22 = new double[3];
			m22[0] = m22[1] = m22[2] = 2.0;

			Complex* m3 = new Complex[2];
			m3[0] = m3[1] = Complex(1.0, 2);
			Complex* m33 = new Complex[3];
			m33[0] = m33[1] = m33[2] = Complex(1.0, 2);


			LinkedList<int> a(m11, 3), a1(m1, 2), *a2;
			LinkedList<double> b(m22, 3), b1(m2, 2), *b2;
			LinkedList<Complex> c(m33, 3), c1(m3, 2), *c2;

			a2 = a.GetSubList(0, 1);
			b2 = b.GetSubList(0, 1);
			c2 = c.GetSubList(0, 1);
			Assert::IsTrue(a1 == *a2 && b1 == *b2 && c1 == *c2);

			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.GetSubList(0, 1); });
			Assert::ExpectException<int>([this] { LinkedList<double> a1; a1.GetSubList(0, 1); });
			Assert::ExpectException<int>([this] { LinkedList<Complex> a1; a1.GetSubList(0, 1); });
			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.GetSubList(0, 1); });
			Assert::ExpectException<int>([this] { LinkedList<double> a1; a1.GetSubList(0, 1); });
			Assert::ExpectException<int>([this] { LinkedList<Complex> a1; a1.GetSubList(0, 1); });

			delete a2;
			delete b2;
			delete c2;
			delete[] m11;
			delete[] m1;
			delete[] m22;
			delete[] m2;
			delete[] m33;
			delete[] m3;
		}

		TEST_METHOD(CE_LinkedList_IsertConcatEraseTests)
		{
			int* m1 = new int[2];
			m1[0] = 1;
			m1[1] = 2;
			int* m11 = new int[3];
			m11[0] = m11[1] = m11[2] = 2;

			LinkedList<int> a(m1, 2), b(m11, 3);

			a.InsertAt(-1, 1);
			Assert::IsTrue(a.get(0) == 1 && a.get(1) == -1 && a.get(2) == 2);
			a.InsertAt(-2, 0);
			Assert::IsTrue(a.get(0) == -2 && a.get(1) == 1 && a.get(2) == -1 && a.get(3) == 2);
			a.InsertAt(-3, 4);
			Assert::IsTrue(a.get(0) == -2 && a.get(1) == 1 && a.get(2) == -1 && a.get(3) == 2 && a.get(4) == -3);

			a.Concat(b);
			Assert::IsTrue(a.get(0) == -2 && a.get(1) == 1 && a.get(2) == -1 && a.get(3) == 2 && a.get(4) == -3 && a.get(5) == 2 && a.get(6) == 2 && a.get(7) == 2);

			a.Erase(7);
			Assert::IsTrue(a.get(0) == -2 && a.get(1) == 1 && a.get(2) == -1 && a.get(3) == 2 && a.get(4) == -3 && a.get(5) == 2 && a.get(6) == 2);
			a.Erase(0);
			a.Erase(1);
			Assert::IsTrue(a.get(0) == 1 && a.get(1) == 2 && a.get(2) == -3 && a.get(3) == 2 && a.get(4) == 2);

			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.InsertAt(1, 1); });
			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.Append(1); a1.InsertAt(1, -1); });
			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.Append(1); a1.InsertAt(10, 11); });
			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.Append(1); a1.Erase(1); });
			Assert::ExpectException<int>([this] { LinkedList<int> a1; a1.Append(1); a1.Erase(-1); });

			delete[] m11;
			delete[] m1;
		}

		TEST_METHOD(CF_LinkedList_MoreConcatTests)
		{
			int *m1 = new int[2], *m2 = new int[3], *m3 = new int[5];
			m3[0] = m1[0] = 0;
			m3[1] = m1[1] = 1;
			m3[2] = m2[0] = 2;
			m3[3] = m2[1] = 3;
			m3[4] = m2[2] = 4;
			LinkedList<int> a(m1, 2), b(m2, 3), c(m3, 5);

			a.Concat(b);
			Assert::IsTrue(a == c);
			b.Concat(a);
			Assert::IsFalse(b == a || b == c);

			delete[] m1;
			delete[] m2;
			delete[] m3;
		}

		TEST_METHOD(CG_LinkedList_IndexOperatorTest)
		{
			int* m1 = new int[2];
			m1[0] = 1;
			m1[1] = 2;

			LinkedList<int> a(m1, 2);

			Assert::IsTrue(a[0] == 1 && a[1] == 2);

			a[0] = 3;
			Assert::IsTrue(a[0] == 3 && a[1] == 2);

			a[1] = 4;
			Assert::IsTrue(a[0] == 3 && a[1] == 4);

			Assert::ExpectException<int>([this] {LinkedList<int> a; a[0]; });
			Assert::ExpectException<int>([this] {int* m1 = new int[2]; m1[0] = 1; m1[1] = 2; LinkedList<int> a(m1, 2); a[2]; });
			Assert::ExpectException<int>([this] {int* m1 = new int[2]; m1[0] = 1; m1[1] = 2; LinkedList<int> a(m1, 2); a[-1]; });

			delete[] m1;
		}

		/*
		* END OF TESTING
		* LinkedList.h
		* STARTING TESTING
		* Sequence.h
		*/

		TEST_METHOD(DA_ArraySequence_InitGetTests)
		{
			{
				ArraySequence<int> a;
				ArraySequence<double> b;
				ArraySequence<Complex> c;
				
				Assert::IsTrue(a.getSize() == 0 && b.getSize() == 0 && c.getSize() == 0);
				Assert::ExpectException<int>([this] {ArraySequence<int> a; a.get(0); });
				Assert::ExpectException<int>([this] {ArraySequence<int> a; a.getFirst(); });
				Assert::ExpectException<int>([this] {ArraySequence<int> a; a.getLast(); });
			}
			{
				int* m = new int[2], *ptr = 0;
				m[0] = 0;
				m[1] = 1;
				ArraySequence<int> a(m, 2), b(ptr, 0);
				Assert::IsTrue(a.get(0) == 0 && a.get(1) == 1);
				Assert::IsTrue(b.getSize() == 0);

				ArraySequence<int> d(a), c(b);
				Assert::IsTrue(a == d && c == b);
				Assert::IsTrue(a.getFirst() == 0 && a.getLast() == 1);

				delete[] m;
			}
			{
				ArraySequence<double> a(3, 10.1);
				Assert::IsTrue(a.getSize() == 3 && a[0] == 10.1 && a[1] == 10.1 && a[2] == 10.1);
				
				ArraySequence<ArraySequence<int>> b(3, ArraySequence<int>(3, int(3)));
				Assert::IsTrue(b.getSize() == 3 && b[0][0] == b[0][1] && b[0][2] && b[0][2] == 3 && b[1][0] == b[1][2] && b[1][2] == 3 && b[2][0] == b[2][2] && b[2][2] == 3);
			}
		}

		TEST_METHOD(DB_ArraySequence_EquationOperatorTest)
		{
			int *m1 = new int[2], *m2 = new int[3];
			m1[0] = 0;
			m1[1] = 1;
			m2[0] = 0;
			m2[1] = 1;
			m2[2] = 2;
			ArraySequence<int> a(m1, 2), b(m2, 3), c(m1, 2), d((int*)0, 0), e((int*)0, 0);

			Assert::IsTrue(a == c && !(a == b) && d == e && !(a == d) && !(b == d));
		}

		TEST_METHOD(DC_ArraySequence_SetInsertAtEraseTests)
		{
			ArraySequence<int> a(2);
			a.set(0, 0);
			a.set(1, 1);
			a.InsertAt(-1, 1);
			Assert::IsTrue(a.getSize() == 3 && a[0] == 0 && a[1] == -1 && a[2] == 1);

			a.InsertAt(-2, 0);
			a.InsertAt(-3, 4);
			Assert::IsTrue(a.getSize() == 5 && a[0] == -2 && a[1] == 0 && a[2] == -1 && a[3] == 1 && a[4] == -3);
			a.Erase(4);
			Assert::IsTrue(a.getSize() == 4 && a[0] == -2 && a[1] == 0 && a[2] == -1 && a[3] == 1);
			a.Erase(0);
			a.Erase(1);
			Assert::IsTrue(a.getSize() == 2 && a[0] == 0 && a[1] == 1);

			Assert::ExpectException<int>([this] {DynamicArray<int> a(2); a.InsertAt(-1, -1); });
			Assert::ExpectException<int>([this] {DynamicArray<int> a(2); a.InsertAt(-1, 3); });
			Assert::ExpectException<int>([this] {DynamicArray<int> a(2); a.Erase(-1); });
			Assert::ExpectException<int>([this] {DynamicArray<int> a(2); a.Erase(2); });
		}

		TEST_METHOD(DD_ArraySequence_IndexOperatorTests)
		{
			ArraySequence<Complex> a(5);
			for (int i = 0; i < 5; i++)
				a[i] = Complex(i);
			for (int i = 0; i < 5; i++)
				Assert::IsTrue(a[i].getReal() == i && a[i].getImag() == 0);

			Assert::ExpectException<int>([this] {ArraySequence<int> a; a[0] = 1; });
			Assert::ExpectException<int>([this] {ArraySequence<int> a(1); a[-1] = 1; });
			Assert::ExpectException<int>([this] {ArraySequence<int> a(1); a[1] = 1; });
		}

		TEST_METHOD(DE_ArraySequence_DifferentTypesTests)
		{
			ArraySequence<int>* m = new ArraySequence<int>[3];
			m[2] = m[1] = m[0] = ArraySequence<int>(3, 3);
			ArraySequence<ArraySequence<int>> a, b(3), c(m, 3), d(3, ArraySequence<int>(3, 4)), e(d);
			
			Assert::IsTrue(a.getSize() == 0);
			Assert::ExpectException<int>([&a]() {a[0][0]; });
			Assert::ExpectException<int>([&a]() {a[0]; });
			Assert::ExpectException<int>([&a]() {a.getFirst(); });
			Assert::ExpectException<int>([&a]() {a.getLast(); });

			Assert::IsTrue(b.getSize() == 3 && b[0].getSize() == 0 && b[1].getSize() == 0 && b[2].getSize() == 0);
			Assert::ExpectException<int>([&b]() {b[0][0]; });
			Assert::ExpectException<int>([&b]() {b[2][0]; });

			Assert::IsTrue(c.getSize() == 3 && c[0][0] == 3 && c[0][1] == 3 && c[0][2] == 3 && c[1][0] == 3 && c[1][2] == 3 && c[2][0] == 3 && c[2][2] == 3 && c[0].getSize() == 3 && c[2].getSize() == 3);
			Assert::ExpectException<int>([&c]() {c[-1]; });
			Assert::ExpectException<int>([&c]() {c[10]; });

			Assert::IsTrue(d.getSize() == 3 && d[0][0] == 4 && d[0][1] == 4 && d[0][2] == 4 && d[1][0] == 4 && d[1][2] == 4 && d[2][0] == 4 && d[2][2] == 4 && d[0].getSize() == 3 && d[2].getSize() == 3);
			Assert::IsTrue(d == e);
			Assert::IsTrue(d.getFirst() == ArraySequence<int>(3, 4));
			Assert::IsTrue(e.getLast() == ArraySequence<int>(3, 4));

			d.set(0, ArraySequence<int>(2, -1));
			d[1] = ArraySequence<int>(1, 1);
			e = d;
			d.Append(ArraySequence<int>(4, -10)); 
			Assert::IsTrue(d[0] == ArraySequence<int>(2, -1) && d[1] == ArraySequence<int>(1, 1) && d[3] == ArraySequence<int>(4, -10));
			a = d;
			d.Erase(d.getSize() - 1);
			Assert::IsTrue(e == d);
			d.InsertAt(ArraySequence<int>(4, -10), 3);
			Assert::IsTrue(a == d);
			Assert::ExpectException<int>([&d]() {d.InsertAt(ArraySequence<int>(1, 1), -1); });
			Assert::ExpectException<int>([&d]() {d.InsertAt(ArraySequence<int>(1, 1), 10); });
			Assert::ExpectException<int>([&d]() {d.Erase(-1); });
			Assert::ExpectException<int>([&d]() {d.Erase(10); });

		}


		TEST_METHOD(EA_LListSequence_InitGetTests)
		{
			{
				LListSequence<int> a;
				LListSequence<double> b;
				LListSequence<Complex> c;

				Assert::IsTrue(a.getSize() == 0 && b.getSize() == 0 && c.getSize() == 0);
				Assert::ExpectException<int>([this] {ArraySequence<int> a; a.get(0); });
				Assert::ExpectException<int>([this] {ArraySequence<int> a; a.getFirst(); });
				Assert::ExpectException<int>([this] {ArraySequence<int> a; a.getLast(); });
			}
			{
				int* m = new int[2], * ptr = 0;
				m[0] = 0;
				m[1] = 1;
				ArraySequence<int> a(m, 2), b(ptr, 0);
				Assert::IsTrue(a.get(0) == 0 && a.get(1) == 1);
				Assert::IsTrue(b.getSize() == 0);

				ArraySequence<int> d(a), c(b);
				Assert::IsTrue(a == d && c == b);
				Assert::IsTrue(a.getFirst() == 0 && a.getLast() == 1);

				delete[] m;
			}
		}

		TEST_METHOD(EB_LListSequence_EquationOperatorTest)
		{
			int* m1 = new int[2], * m2 = new int[3];
			m1[0] = 0;
			m1[1] = 1;
			m2[0] = 0;
			m2[1] = 1;
			m2[2] = 2;
			LListSequence<int> a(m1, 2), b(m2, 3), c(m1, 2), d((int*)0, 0), e((int*)0, 0);

			Assert::IsTrue(a == c && !(a == b) && d == e && !(a == d) && !(b == d));
		}

		TEST_METHOD(EC_LListSequence_SetInsertAtEraseTests)
		{
			LListSequence<int> a(2);
			a.set(0, 0);
			a.set(1, 1);
			a.InsertAt(-1, 1);
			Assert::IsTrue(a.getSize() == 3 && a[0] == 0 && a[1] == -1 && a[2] == 1);

			a.InsertAt(-2, 0);
			a.InsertAt(-3, 4);
			Assert::IsTrue(a.getSize() == 5 && a[0] == -2 && a[1] == 0 && a[2] == -1 && a[3] == 1 && a[4] == -3);
			a.Erase(4);
			Assert::IsTrue(a.getSize() == 4 && a[0] == -2 && a[1] == 0 && a[2] == -1 && a[3] == 1);
			a.Erase(0);
			a.Erase(1);
			Assert::IsTrue(a.getSize() == 2 && a[0] == 0 && a[1] == 1);

			Assert::ExpectException<int>([this] {LListSequence<int> a(2); a.InsertAt(-1, -1); });
			Assert::ExpectException<int>([this] {LListSequence<int> a(2); a.InsertAt(-1, 3); });
			Assert::ExpectException<int>([this] {LListSequence<int> a(2); a.Erase(-1); });
			Assert::ExpectException<int>([this] {LListSequence<int> a(2); a.Erase(2); });
		}

		TEST_METHOD(ED_LListSequence_IndexOperatorTests)
		{
			LListSequence<Complex> a(5);
			for (int i = 0; i < 5; i++)
				a[i] = Complex(i);
			for (int i = 0; i < 5; i++)
				Assert::IsTrue(a[i].getReal() == i && a[i].getImag() == 0);

			Assert::ExpectException<int>([this] {LListSequence<int> a; a[0] = 1; });
			Assert::ExpectException<int>([this] {LListSequence<int> a(1); a[-1] = 1; });
			Assert::ExpectException<int>([this] {LListSequence<int> a(1); a[1] = 1; });
		}

		TEST_METHOD(EE_LListSequence_getSubSequenceConcatTests)
		{
			int* m = new int[5];
			m[0] = 0;
			m[1] = 1;
			m[2] = 2;
			m[3] = 3;
			m[4] = 4;
			LListSequence<int> a(10, 4), b(m, 5), *c = a.getSubSequence(1, 4), *d = b.getSubSequence(1, 3);
			Assert::IsTrue(c->getSize() == 4 && (*c)[0] == (*c)[1] && (*c)[1] == (*c)[2] && (*c)[2] == (*c)[3] && (*c)[3] == 4);
			Assert::IsTrue(d->getSize() == 3 && (*d)[0] == 1 && (*d)[1] == 2 && (*d)[2] == 3);
			c->Concat(*d);
			Assert::IsTrue((*c)[0] == (*c)[1] && (*c)[1] == (*c)[2] && (*c)[2] == (*c)[3] && (*c)[3] == 4 && (*c)[4] == 1 && (*c)[5] == 2 && (*c)[6] == 3 && c->getSize() == 7);

			Assert::ExpectException<int>([&a]() {a.getSubSequence(0, 1000); });
			delete c;
			delete d;
		}

		TEST_METHOD(EF_LListSequence_DifferentTypesTests)
		{
			LListSequence<int>* m = new LListSequence<int>[3];
			m[2] = m[1] = m[0] = LListSequence<int>(3, 3);
			LListSequence<LListSequence<int>> a, b(3), c(m, 3), d(3, LListSequence<int>(3, 4)), e(d);

			Assert::IsTrue(a.getSize() == 0);
			Assert::ExpectException<int>([&a]() {a[0][0]; });
			Assert::ExpectException<int>([&a]() {a[0]; });
			Assert::ExpectException<int>([&a]() {a.getFirst(); });
			Assert::ExpectException<int>([&a]() {a.getLast(); });

			Assert::IsTrue(b.getSize() == 3 && b[0].getSize() == 0 && b[1].getSize() == 0 && b[2].getSize() == 0);
			Assert::ExpectException<int>([&b]() {b[0][0]; });
			Assert::ExpectException<int>([&b]() {b[2][0]; });

			Assert::IsTrue(c.getSize() == 3 && c[0][0] == 3 && c[0][1] == 3 && c[0][2] == 3 && c[1][0] == 3 && c[1][2] == 3 && c[2][0] == 3 && c[2][2] == 3 && c[0].getSize() == 3 && c[2].getSize() == 3);
			Assert::ExpectException<int>([&c]() {c[-1]; });
			Assert::ExpectException<int>([&c]() {c[10]; });

			Assert::IsTrue(d.getSize() == 3 && d[0][0] == 4 && d[0][1] == 4 && d[0][2] == 4 && d[1][0] == 4 && d[1][2] == 4 && d[2][0] == 4 && d[2][2] == 4 && d[0].getSize() == 3 && d[2].getSize() == 3);
			Assert::IsTrue(d == e);
			Assert::IsTrue(d.getFirst() == LListSequence<int>(3, 4));
			Assert::IsTrue(e.getLast() == LListSequence<int>(3, 4));

			d.set(0, LListSequence<int>(2, -1));
			Assert::IsTrue(d[0][0] == -1);
			d[1] = LListSequence<int>(1, 1);
			e = d;
			d.Append(LListSequence<int>(4, -10));
			Assert::IsTrue(d[0] == LListSequence<int>(2, -1));
			Assert::IsTrue(d[1] == LListSequence<int>(1, 1));
			Assert::IsTrue(d[3] == LListSequence<int>(4, -10));
			a = d;
			d.Erase(d.getSize() - 1);
			Assert::IsTrue(e == d);
			d.InsertAt(LListSequence<int>(4, -10), 3);
			Assert::IsTrue(a == d);
			Assert::ExpectException<int>([&d]() {d.InsertAt(LListSequence<int>(1, 1), -1); });
			Assert::ExpectException<int>([&d]() {d.InsertAt(LListSequence<int>(1, 1), 10); });
			Assert::ExpectException<int>([&d]() {d.Erase(-1); });
			Assert::ExpectException<int>([&d]() {d.Erase(10); });

		}

		/*
		* END OF TESTING
		* Sequence.h
		* STARTING TESTING
		* MapReduce.h
		*/

		TEST_METHOD(FA_MapReduceTests)
		{
			{
				ArraySequence<int>* a = new ArraySequence<int>(10, 1), * b;
				b = Map(a, (std::function<int(int)>)[](int x) {return x * 10; });
				Assert::IsTrue(*b == ArraySequence<int>(10, 10));
				int* c = Reduce(b, (std::function<int(int, int)>)[](int x, int y) {return x + y; }, 0);
				Assert::IsTrue(*c == 100);
				delete c;
				c = MapReduce(a, b, (std::function<int(int, int, int)>)[](int x, int y, int z) {return x + y * z; }, 0);
				Assert::IsTrue(*c == 100);
				delete a;
				delete b;
				delete c;
			}
			{
				ArraySequence<int>* a = new ArraySequence<int>(5, 2), * b = new ArraySequence<int>(5, 3), * c;
				c = Map(a, b, (std::function<int(int, int)>)[](int x1, int x2) {return x1 + x2; });
				Assert::IsTrue(*c == ArraySequence<int>(5, 5));
				delete a, b, c;
			}
			{
				ArraySequence<ArraySequence<int>>* a = new ArraySequence<ArraySequence<int>>(4, ArraySequence<int>(3, 2));
				ArraySequence<ArraySequence<int>>* b = new ArraySequence<ArraySequence<int>>(4, ArraySequence<int>(3, 3)), *c;
				std::function<ArraySequence<int>(ArraySequence<int>, ArraySequence<int>)> f = [](ArraySequence<int> x, ArraySequence<int> y) 
				{return *Map(&x, &y, (std::function<int(int, int)>)[](int a, int b) {return a + b; }); };
				c = Map(a, b, f);
				Assert::IsTrue(*c == ArraySequence<ArraySequence<int>>(4, ArraySequence<int>(3, 5)));

				delete a;
				delete b;
			}
		}
		/*
		* END OF TESTING
		* MapReduce.h
		* STARTING TESTING
		* Matrix.h
		*/

		TEST_METHOD(GA_Matrix_InitOperationsTests)
		{
			{
				SquareMatrix <int> a, b(2), c(2, 2);
				Assert::IsTrue(a.getSize() == 0 && b.getSize() == 2 && c.getSize() == 2);
				Assert::IsTrue(b.get(0, 0) == 0 && b.get(0, 1) == 0 && b.get(1, 0) == 0 && b.get(1, 1) == 0);
				Assert::IsTrue(c.get(0, 0) == 2 && c.get(0, 1) == 2 && c.get(1, 0) == 2 && c.get(1, 1) == 2);
				b + c;
				Assert::IsTrue(b.get(0, 0) == 2 && b.get(0, 1) == 2 && b.get(1, 0) == 2 && b.get(1, 1) == 2);
				Assert::ExpectException<int>([&a, &b]() {a + b; });
				Assert::ExpectException<int>([&a, &b]() {b + a; });
				b * 3;
				Assert::IsTrue(b.get(0, 0) == 6 && b.get(0, 1) == 6 && b.get(1, 0) == 6 && b.get(1, 1) == 6);
				b* c;
				Assert::IsTrue(b.get(0, 0) == 24 && b.get(0, 1) == 24 && b.get(1, 0) == 24 && b.get(1, 1) == 24);
				Assert::ExpectException<int>([&a, &c]() {a* c; });
				Assert::ExpectException<int>([&a, &c]() {c* a; });
			}
			{
				SquareMatrix<Complex> a, b(2), c(2, Complex(2));
				Assert::IsTrue(a.getSize() == 0 && b.getSize() == 2 && c.getSize() == 2);
				Assert::IsTrue(b.get(0, 0) == Complex(0) && b.get(0, 1) == Complex(0) && b.get(1, 0) == Complex(0) && b.get(1, 1) == Complex(0));
				Assert::IsTrue(c.get(0, 0) == Complex(2) && c.get(0, 1) == Complex(2) && c.get(1, 0) == Complex(2) && c.get(1, 1) == Complex(2));
				b + c;
				Assert::IsTrue(b.get(0, 0) == Complex(2) && b.get(0, 1) == Complex(2) && b.get(1, 0) == Complex(2) && b.get(1, 1) == Complex(2));
				Assert::ExpectException<int>([&a, &b]() {a + b; });
				Assert::ExpectException<int>([&a, &b]() {b + a; });
				b * 3;
				Assert::IsTrue(b.get(0, 0) == Complex(6) && b.get(0, 1) == Complex(6) && b.get(1, 0) == Complex(6) && b.get(1, 1) == Complex(6));
				b* c;
				Assert::IsTrue(b.get(0, 0) == Complex(24) && b.get(0, 1) == Complex(24) && b.get(1, 0) == Complex(24) && b.get(1, 1) == Complex(24));
				Assert::ExpectException<int>([&a, &c]() {a* c; });
				Assert::ExpectException<int>([&a, &c]() {c* a; });
			}
		}

		TEST_METHOD(GB_Matrix_BasicOperationsTest)
		{
			SquareMatrix<int> a(3, 2);
			a.sumColumns(0, 1);
			Assert::IsTrue(a.getColumn(0) == ArraySequence<int>(3, 4));
			Assert::ExpectException<int>([&a]() {a.sumColumns(100, 10); });
			Assert::ExpectException<int>([&a]() {a.sumColumns(0, 10); });
			a.sumRows(0, 1);
			Assert::IsTrue(a.get(0, 0) == 8 && a.get(0, 1) == 4 && a.get(1, 0) == 4 && a.get(1, 1) == 2 && a.get(0, 2) == 4 && a.get(1, 2) == 2);
			Assert::ExpectException<int>([&a]() {a.sumRows(10, 10); });
			Assert::ExpectException<int>([&a]() {a.sumRows(0, 10); });

			a = SquareMatrix<int>(2, 2);
			a.multiplyColumnBy(0, 3);
			Assert::IsTrue(a.getColumn(0) == ArraySequence<int>(2, 6));
			Assert::ExpectException<int>([&a]() {a.multiplyColumnBy(100, 1); });
			a.multiplyColumnBy(1, 3);
			Assert::IsTrue(a == SquareMatrix<int>(2, 6));
			a.multiplyRowBy(0, 2);
			Assert::IsTrue(a.getRow(0) == ArraySequence<int>(2, 12) && a.getRow(1) == ArraySequence<int>(2, 6));
			a.ChangeRows(0, 1);
			Assert::IsTrue(a.getRow(1) == ArraySequence<int>(2, 12) && a.getRow(0) == ArraySequence<int>(2, 6));

			a = SquareMatrix<int>(2, 2);
			a.set(-1, 0, 0);
			a.ChangeColumns(0, 1);
			Assert::IsTrue(a.getColumn(0) == ArraySequence<int>(2, 2) && a.get(0, 1) == -1 && a.get(1, 1) == 2);
		}

		TEST_METHOD(GC_Matrix_EquationOperatorTest)
		{
			SquareMatrix<int> a, b(2), c(2), d(2, 2), e(3, 3), f(3, 3);
			Assert::IsFalse(a == b || b == d || a == c || b == e);
			Assert::IsTrue(b == c && e == f);
			f.set(-1, 1, 1);
			Assert::IsTrue(e != f);
			Assert::IsTrue(f != c && f != a);
		}
		
		TEST_METHOD(GD_Matrix_GetsTests)
		{
			SquareMatrix<int> a(3, 3);
			Assert::IsTrue(a.getColumn(0) == ArraySequence<int>(3, 3));
			Assert::IsTrue(a.getColumn(1) == ArraySequence<int>(3, 3));
			Assert::IsTrue(a.getColumn(2) == ArraySequence<int>(3, 3));
			a.set(-1, 1, 0);
			ArraySequence<int> x = a.getColumn(0);
			Assert::IsTrue(x[0] == 3 && x[1] == -1 && x[2] == 3);
			Assert::ExpectException<int>([&a]() {a.getColumn(100); });
			Assert::ExpectException<int>([&a]() {a.set(100, 100, 100); });

			x = a.getRow(0);
			Assert::IsTrue(x == ArraySequence<int>(3, 3));
			x = a.getRow(1);
			Assert::IsTrue(x.getSize() == 3 && x[0] == -1 && x[1] == 3 && x[2] == 3);
			Assert::ExpectException<int>([&a]() {a.getRow(1000); });
		}

		TEST_METHOD(GE_Matrix_NormTests)
		{
			{
				SquareMatrix<int> a(2);
				a.set(0, 0, 0);
				a.set(1, 0, 1);
				a.set(2, 1, 0);
				a.set(3, 1, 1);
				Assert::IsTrue(a.normaMax() == 3);
			}
			{
				SquareMatrix<Complex> a(2, Complex(4, 3));
				a.set(Complex(0), 0, 0);
				a.set(Complex(1, 2), 0, 1);
				a.set(Complex(-1, 2), 1, 1);
				Assert::IsTrue(a.normaMax() == Complex(4, 3).Module());
				Assert::ExpectException<int>([]() {SquareMatrix<int> a; a.normaMax(); });
			}
		}


		TEST_METHOD(GG_Matrix_InitOperationsTests)
		{
			{
				ListedSquareMatrix <int> a, b(2), c(2, 2);
				Assert::IsTrue(a.getSize() == 0 && b.getSize() == 2 && c.getSize() == 2);
				Assert::IsTrue(b.get(0, 0) == 0 && b.get(0, 1) == 0 && b.get(1, 0) == 0 && b.get(1, 1) == 0);
				Assert::IsTrue(c.get(0, 0) == 2 && c.get(0, 1) == 2 && c.get(1, 0) == 2 && c.get(1, 1) == 2);
				b + c;
				Assert::IsTrue(b.get(0, 0) == 2 && b.get(0, 1) == 2 && b.get(1, 0) == 2 && b.get(1, 1) == 2);
				Assert::ExpectException<int>([&a, &b]() {a + b; });
				Assert::ExpectException<int>([&a, &b]() {b + a; });
				b * 3;
				Assert::IsTrue(b.get(0, 0) == 6 && b.get(0, 1) == 6 && b.get(1, 0) == 6 && b.get(1, 1) == 6);
				b* c;
				Assert::IsTrue(b.get(0, 0) == 24 && b.get(0, 1) == 24 && b.get(1, 0) == 24 && b.get(1, 1) == 24);
				Assert::ExpectException<int>([&a, &c]() {a* c; });
				Assert::ExpectException<int>([&a, &c]() {c* a; });
			}
			{
				ListedSquareMatrix<Complex> a, b(2), c(2, Complex(2));
				Assert::IsTrue(a.getSize() == 0 && b.getSize() == 2 && c.getSize() == 2);
				Assert::IsTrue(b.get(0, 0) == Complex(0) && b.get(0, 1) == Complex(0) && b.get(1, 0) == Complex(0) && b.get(1, 1) == Complex(0));
				Assert::IsTrue(c.get(0, 0) == Complex(2) && c.get(0, 1) == Complex(2) && c.get(1, 0) == Complex(2) && c.get(1, 1) == Complex(2));
				b + c;
				Assert::IsTrue(b.get(0, 0) == Complex(2) && b.get(0, 1) == Complex(2) && b.get(1, 0) == Complex(2) && b.get(1, 1) == Complex(2));
				Assert::ExpectException<int>([&a, &b]() {a + b; });
				Assert::ExpectException<int>([&a, &b]() {b + a; });
				b * 3;
				Assert::IsTrue(b.get(0, 0) == Complex(6) && b.get(0, 1) == Complex(6) && b.get(1, 0) == Complex(6) && b.get(1, 1) == Complex(6));
				b* c;
				Assert::IsTrue(b.get(0, 0) == Complex(24) && b.get(0, 1) == Complex(24) && b.get(1, 0) == Complex(24) && b.get(1, 1) == Complex(24));
				Assert::ExpectException<int>([&a, &c]() {a* c; });
				Assert::ExpectException<int>([&a, &c]() {c* a; });
			}
		}

		TEST_METHOD(GH_Matrix_BasicOperationsTest)
		{
			ListedSquareMatrix<int> a(3, 2);
			a.sumColumns(0, 1);
			Assert::IsTrue(a.getColumn(0) == LListSequence<int>(3, 4));
			Assert::ExpectException<int>([&a]() {a.sumColumns(100, 10); });
			Assert::ExpectException<int>([&a]() {a.sumColumns(0, 10); });
			a.sumRows(0, 1);
			Assert::IsTrue(a.get(0, 0) == 8 && a.get(0, 1) == 4 && a.get(1, 0) == 4 && a.get(1, 1) == 2 && a.get(0, 2) == 4 && a.get(1, 2) == 2);
			Assert::ExpectException<int>([&a]() {a.sumRows(10, 10); });
			Assert::ExpectException<int>([&a]() {a.sumRows(0, 10); });

			a = ListedSquareMatrix<int>(2, 2);
			a.multiplyColumnBy(0, 3);
			Assert::IsTrue(a.getColumn(0) == LListSequence<int>(2, 6));
			Assert::ExpectException<int>([&a]() {a.multiplyColumnBy(100, 1); });
			a.multiplyColumnBy(1, 3);
			Assert::IsTrue(a == ListedSquareMatrix<int>(2, 6));
			a.multiplyRowBy(0, 2);
			Assert::IsTrue(a.getRow(0) == LListSequence<int>(2, 12) && a.getRow(1) == LListSequence<int>(2, 6));
			a.ChangeRows(0, 1);
			Assert::IsTrue(a.getRow(1) == LListSequence<int>(2, 12) && a.getRow(0) == LListSequence<int>(2, 6));

			a = ListedSquareMatrix<int>(2, 2);
			a.set(-1, 0, 0);
			a.ChangeColumns(0, 1);
			Assert::IsTrue(a.getColumn(0) == LListSequence<int>(2, 2) && a.get(0, 1) == -1 && a.get(1, 1) == 2);
		}

		TEST_METHOD(GI_Matrix_EquationOperatorTest)
		{
			ListedSquareMatrix<int> a, b(2), c(2), d(2, 2), e(3, 3), f(3, 3);
			Assert::IsFalse(a == b || b == d || a == c || b == e);
			Assert::IsTrue(b == c && e == f);
			f.set(-1, 1, 1);
			Assert::IsTrue(e != f);
			Assert::IsTrue(f != c && f != a);
		}

		TEST_METHOD(GJ_Matrix_GetsTests)
		{
			ListedSquareMatrix<int> a(3, 3);
			Assert::IsTrue(a.getColumn(0) == LListSequence<int>(3, 3));
			Assert::IsTrue(a.getColumn(1) == LListSequence<int>(3, 3));
			Assert::IsTrue(a.getColumn(2) == LListSequence<int>(3, 3));
			a.set(-1, 1, 0);
			LListSequence<int> x = a.getColumn(0);
			Assert::IsTrue(x[0] == 3 && x[1] == -1 && x[2] == 3);
			Assert::ExpectException<int>([&a]() {a.getColumn(100); });
			Assert::ExpectException<int>([&a]() {a.set(100, 100, 100); });

			x = a.getRow(0);
			Assert::IsTrue(x == LListSequence<int>(3, 3));
			x = a.getRow(1);
			Assert::IsTrue(x.getSize() == 3 && x[0] == -1 && x[1] == 3 && x[2] == 3);
			Assert::ExpectException<int>([&a]() {a.getRow(1000); });
		}

		TEST_METHOD(GK_Matrix_NormTests)
		{
			{
				ListedSquareMatrix<int> a(2);
				a.set(0, 0, 0);
				a.set(1, 0, 1);
				a.set(2, 1, 0);
				a.set(3, 1, 1);
				Assert::IsTrue(a.normaMax() == 3);
			}
			{
				ListedSquareMatrix<Complex> a(2, Complex(4, 3));
				a.set(Complex(0), 0, 0);
				a.set(Complex(1, 2), 0, 1);
				a.set(Complex(-1, 2), 1, 1);
				Assert::IsTrue(a.normaMax() == Complex(4, 3).Module());
				Assert::ExpectException<int>([]() {SquareMatrix<int> a; a.normaMax(); });
			}
		}
	};
}
